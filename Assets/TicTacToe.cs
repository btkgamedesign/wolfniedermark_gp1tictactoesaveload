﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TicTacToe : MonoBehaviour
{
    public int playerTurn; //1 == player 1 X, 2 == player 2 O
    public int turnsDone;
    public bool gameOver;

    public Text winText;
    public Text playerTurnText;
    public GameObject restartButton;

    public enum GridState { empty, o, x };

    [System.Serializable] public class Tile
    {
        public GridState state;
    }

    public Tile[] tiles;
    public GameObject[] buttons;

    public void Start()
    {
        playerTurn = 1;
        turnsDone = 0;
        gameOver = false;

        foreach (Tile tile in tiles)
        {
            tile.state = GridState.empty;
        }

        //buttons = GameObject.FindGameObjectsWithTag("GameController");

        foreach (GameObject button in buttons)
        {
            button.GetComponentInChildren<Text>().text = " ";
        }

        restartButton.SetActive(false);
        winText.text = " ";
        playerTurnText.text = "Player " + playerTurn + "'s turn";
    }


    //the buttons need a reference to this, and give their respective number for the corresponding index of the array tiles[]
    public void UpdateTileState(int tileIndex)
    {
        Tile currentTile = tiles[tileIndex];

        if (playerTurn == 1 && currentTile.state == GridState.empty && !gameOver)
        {
            currentTile.state = GridState.x;
            buttons[tileIndex].GetComponentInChildren<Text>().text = "x";
            CheckForWincondition();
            playerTurn++;
            turnsDone++;
        }
        else if (playerTurn == 2 && currentTile.state == GridState.empty && !gameOver)
        {
            currentTile.state = GridState.o;
            buttons[tileIndex].GetComponentInChildren<Text>().text = "o";
            CheckForWincondition();
            playerTurn--;
            turnsDone++;
        }
        playerTurnText.text = "Player " + playerTurn + "'s turn";

        if (turnsDone >= 9 && !gameOver)
        {
            winText.text = "Remi!";
            restartButton.SetActive(true);
        }
    }

    public void SaveGame()
    {
        PlayerPrefs.SetString("oneone", tiles[0].state.ToString());
        PlayerPrefs.SetString("onetwo", tiles[1].state.ToString());
        PlayerPrefs.SetString("onethree", tiles[2].state.ToString());
        PlayerPrefs.SetString("twoone", tiles[3].state.ToString());
        PlayerPrefs.SetString("twotwo", tiles[4].state.ToString());
        PlayerPrefs.SetString("twothree", tiles[5].state.ToString());
        PlayerPrefs.SetString("threeone", tiles[6].state.ToString());
        PlayerPrefs.SetString("threetwo", tiles[7].state.ToString());
        PlayerPrefs.SetString("threethree", tiles[8].state.ToString());
        PlayerPrefs.SetInt("playerTurn", playerTurn);
        PlayerPrefs.SetInt("turnsDone", turnsDone);
    }

    public void LoadGame()
    {
        Start();
        tiles[0].state = (GridState)System.Enum.Parse(typeof(GridState), PlayerPrefs.GetString("oneone"));
        if (tiles[0].state != GridState.empty)
        {
            buttons[0].GetComponentInChildren<Text>().text = tiles[0].state.ToString();
        }
        tiles[1].state = (GridState)System.Enum.Parse(typeof(GridState), PlayerPrefs.GetString("onetwo"));
        if (tiles[1].state != GridState.empty)
        {
            buttons[1].GetComponentInChildren<Text>().text = tiles[1].state.ToString();
        }
        tiles[2].state = (GridState)System.Enum.Parse(typeof(GridState), PlayerPrefs.GetString("onethree"));
        if (tiles[2].state != GridState.empty)
        {
            buttons[2].GetComponentInChildren<Text>().text = tiles[2].state.ToString();
        }
        tiles[3].state = (GridState)System.Enum.Parse(typeof(GridState), PlayerPrefs.GetString("twoone"));
        if (tiles[3].state != GridState.empty)
        {
            buttons[3].GetComponentInChildren<Text>().text = tiles[3].state.ToString();
        }
        tiles[4].state = (GridState)System.Enum.Parse(typeof(GridState), PlayerPrefs.GetString("twotwo"));
        if (tiles[4].state != GridState.empty)
        {
            buttons[4].GetComponentInChildren<Text>().text = tiles[4].state.ToString();
        }
        tiles[5].state = (GridState)System.Enum.Parse(typeof(GridState), PlayerPrefs.GetString("twothree"));
        if (tiles[5].state != GridState.empty)
        {
            buttons[5].GetComponentInChildren<Text>().text = tiles[5].state.ToString();
        }
        tiles[6].state = (GridState)System.Enum.Parse(typeof(GridState), PlayerPrefs.GetString("threeone"));
        if (tiles[6].state != GridState.empty)
        {
            buttons[6].GetComponentInChildren<Text>().text = tiles[6].state.ToString();
        }
        tiles[7].state = (GridState)System.Enum.Parse(typeof(GridState), PlayerPrefs.GetString("threetwo"));
        if (tiles[7].state != GridState.empty)
        {
            buttons[7].GetComponentInChildren<Text>().text = tiles[7].state.ToString();
        }
        tiles[8].state = (GridState)System.Enum.Parse(typeof(GridState), PlayerPrefs.GetString("threethree"));
        if (tiles[8].state != GridState.empty)
        {
            buttons[8].GetComponentInChildren<Text>().text = tiles[8].state.ToString();
        }

        playerTurn = PlayerPrefs.GetInt("playerTurn");
        turnsDone = PlayerPrefs.GetInt("turnsDone");
        playerTurnText.text = "Player " + playerTurn + "'s turn";
    }



    /// <summary>
    /// Winning Conditions
    /// </summary>
    public void CheckForWincondition()
    {
        //////// Player X
        // >
        if (tiles[0].state == GridState.x && tiles[1].state == GridState.x && tiles[2].state == GridState.x)
        {
            restartButton.SetActive(true);
            winText.text = "Player 1 won!";
            gameOver = true;
        }
        if (tiles[3].state == GridState.x && tiles[4].state == GridState.x && tiles[5].state == GridState.x)
        {
            restartButton.SetActive(true);
            winText.text = "Player 1 won!";
            gameOver = true;
        }
        if (tiles[6].state == GridState.x && tiles[7].state == GridState.x && tiles[8].state == GridState.x)
        {
            restartButton.SetActive(true);
            winText.text = "Player 1 won!";
            gameOver = true;
        }

        // v
        if (tiles[0].state == GridState.x && tiles[3].state == GridState.x && tiles[6].state == GridState.x)
        {
            restartButton.SetActive(true);
            winText.text = "Player 1 won!";
            gameOver = true;
        }
        if (tiles[1].state == GridState.x && tiles[4].state == GridState.x && tiles[7].state == GridState.x)
        {
            restartButton.SetActive(true);
            winText.text = "Player 1 won!";
            gameOver = true;
        }
        if (tiles[2].state == GridState.x && tiles[5].state == GridState.x && tiles[8].state == GridState.x)
        {
            restartButton.SetActive(true);
            winText.text = "Player 1 won!";
            gameOver = true;
        }
        // X
        if (tiles[0].state == GridState.x && tiles[4].state == GridState.x && tiles[8].state == GridState.x)
        {
            restartButton.SetActive(true);
            winText.text = "Player 1 won!";
            gameOver = true;
        }
        if (tiles[2].state == GridState.x && tiles[4].state == GridState.x && tiles[6].state == GridState.x)
        {
            restartButton.SetActive(true);
            winText.text = "Player 1 won!";
            gameOver = true;
        }

        ////////// Player O
        // >
        if (tiles[0].state == GridState.o && tiles[1].state == GridState.o && tiles[2].state == GridState.o)
        {
            restartButton.SetActive(true);
            winText.text = "Player 2 won!";
            gameOver = true;
        }
        if (tiles[3].state == GridState.o && tiles[4].state == GridState.o && tiles[5].state == GridState.o)
        {
            restartButton.SetActive(true);
            winText.text = "Player 2 won!";
            gameOver = true;
        }
        if (tiles[6].state == GridState.o && tiles[7].state == GridState.o && tiles[8].state == GridState.o)
        {
            restartButton.SetActive(true);
            winText.text = "Player 2 won!";
            gameOver = true;
        }
        // v
        if (tiles[0].state == GridState.o && tiles[3].state == GridState.o && tiles[6].state == GridState.o)
        {
            restartButton.SetActive(true);
            winText.text = "Player 1 won!";
            gameOver = true;
        }
        if (tiles[1].state == GridState.o && tiles[4].state == GridState.o && tiles[7].state == GridState.o)
        {
            restartButton.SetActive(true);
            winText.text = "Player 1 won!";
            gameOver = true;
        }
        if (tiles[2].state == GridState.o && tiles[5].state == GridState.o && tiles[8].state == GridState.o)
        {
            restartButton.SetActive(true);
            winText.text = "Player 1 won!";
            gameOver = true;
        }
        // X
        if (tiles[0].state == GridState.o && tiles[4].state == GridState.o && tiles[8].state == GridState.o)
        {
            restartButton.SetActive(true);
            winText.text = "Player 2 won!";
            gameOver = true;
        }
        if (tiles[2].state == GridState.o && tiles[4].state == GridState.o && tiles[6].state == GridState.o)
        {
            restartButton.SetActive(true);
            winText.text = "Player 2 won!";
            gameOver = true;
        }
    }
}
